export function callTo(phone) {
  window.plugins.CallNumber.callNumber(
    // succesful
    function () {
    },
    // error
    function (error) {
      console.error('callTo error', error)
      // this.$f7.dialog.alert('Sorry. ' + error)
    },
    phone,
    false
  )
}

export function sendSms(phone, text) {
  const options = {
    android: {
      intent: 'INTENT',
    },
  }

  sms.send(
    phone,
    text,
    options,
    // succesful
    function () {
    },
    // error
    function (error) {
      console.error('sendSms error', error)
      // this.$f7.dialog.alert('Sorry. ' + error)
    }
  )
}
