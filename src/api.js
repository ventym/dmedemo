import 'whatwg-fetch'

export const apiFetchEmployeesList = async () => {
  let response = await fetch('https://randomuser.me/api/?noinfo&results=1000&inc=name,phone,email,picture', {
    method: 'GET',
    headers: {
      'Content-Type': 'text/json',
    },
  })
  response = await response.json()
  if (response.error) {
    throw new Error('API error')
  }

  return response.results
}
