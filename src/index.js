import Vue from 'vue'
import Framework7 from 'framework7/dist/framework7.esm.bundle.js'
import Framework7Vue from 'framework7-vue/dist/framework7-vue.esm.bundle.js'
import Framework7Styles from 'framework7/dist/css/framework7.css'

import IconsStyles from './css/icons.css'
import AppStyles from './css/app.css'

import App from './app'
import routes from './routes.js'
import { callTo, sendSms } from './utils'


const app = {
  initialize: function () {
    document.addEventListener('backbutton', this.onBackButton, false)
    document.addEventListener('deviceready', this.startApp, false)
  },

  onBackButton: () => {
    // TODO
  },

  startApp: () => {
    Vue.use(Framework7Vue, Framework7)

    new Vue({
      el: '#app',
      template: '<app/>',
      framework7: {
        id: 'com.dme.demo',
        name: 'DmeDemo',
        theme: 'auto',
        routes,
      },
      data: {
        employees: [],
      },
      methods: {
        callTo,
        sendSms,
      },
      components: {
        app: App
      },
    })

  },
}

app.initialize()