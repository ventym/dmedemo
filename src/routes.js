import ListScreen from './pages/list.vue'
import CardScreen from './pages/card.vue'
import ErrorScreen from './pages/error.vue'
import DefaultScreen from './pages/404.vue'

import { apiFetchEmployeesList } from './api'

export default [
  {
    path: '/list',
    async async(routeTo, routeFrom, resolve, reject) {
      try {
        this.app.preloader.show()

        this.app.data.employees = await apiFetchEmployeesList()

        this.app.preloader.hide()
        resolve({ component: ListScreen })
      } catch(error) {
        console.error('apiFetchEmployeesList error', error)
        this.app.preloader.hide()
        resolve({ component: ErrorScreen })
      }
    },
  },
  {
    path: '/card/:index',
    component: CardScreen,
  },
  {
    path: '(.*)',
    component: DefaultScreen,
  },
]
